<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('dark_magic_router_homepage', new Route('/', array(
    '_controller' => 'DarkMagicRouterBundle:Default:welcome',
)));

$collection->add('dark_magic_router_action', new Route('/{bundle}/{controller}/{action}', array(
    '_controller' => 'DarkMagicRouterBundle:Default:routing',
), [
    'bundle' => '[a-zA-Z0-9_]+',
    'controller' => '[a-zA-Z0-9_]+',
    'action' => '[a-zA-Z0-9_]+',
]));

return $collection;
