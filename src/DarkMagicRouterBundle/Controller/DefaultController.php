<?php

namespace DarkMagicRouterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    protected $containerBuilder;
    protected $servicesList;

    public function welcomeAction(Request $request)
    {
        $this->fetchServicesControllers();
        $controllers = $this->getControllersList();

        return $this->render('DarkMagicRouterBundle:Default:index.html.twig', [
            'controllers' => $controllers,
        ]);
    }

    public function routingAction(Request $request)
    {
        $bundle = $request->attributes->get('bundle');
        $controller = $request->attributes->get('controller');
        $action = $request->attributes->get('action');
        $this->fetchServicesControllers();
        $controllers = $this->getControllersList();
//          Uncomment next line to get another (more primitive) variant
//        return $this->renderWithTwig($controllers, $bundle, $controller, $action);

        try {
            return $this->callController($request, $controllers, $bundle, $controller, $action);
        } catch (\BadMethodCallException $e) {
            return $this->render('DarkMagicRouterBundle:Default:index.html.twig', [
                'controllers' => $controllers,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * @return array
     */
    protected function getControllersList()
    {
        $matches = [];
        $bundles = $this->container->getParameter('kernel.bundles');
        $controllers = [];
        foreach ($bundles as $bundle) {
            $reflection = new \ReflectionClass($bundle);
            $controllerDirectory = dirname($reflection->getFileName()) . '/Controller';
            if (file_exists($controllerDirectory)) {
                $d = dir($controllerDirectory);
                while (false !== ($entry = $d->read())) {
                    if (preg_match("/^([A-Z0-9-_]+)Controller.php/i", $entry, $matches)) {
                        $bundleSlices = explode('\\', $bundle);
                        $bundleShort = array_pop($bundleSlices);
                        $current = [
                            'bundle' => $bundle,
                            'bundle_short' => $bundleShort ,//,substr($bundleShort, 0, strlen($bundleShort) - 6),
                            'class' => $reflection->getNamespaceName() . '\Controller\\' . $matches[1] . 'Controller',
                            'controller' => $matches[1],
                            'file' => $controllerDirectory . '/' . $entry,
                        ];
                        $class = $current['class'];
                        $current['service'] = array_search($class, $this->servicesList);
                        $current['actions'] = $this->getControllerActions($class);
                        $controllers[] = $current;
                    }
                }
                $d->close();
            }
        }

        return $controllers;
    }

    /**
     * @param $class
     * @return array
     */
    protected function getControllerActions($class)
    {
        if (class_exists($class)) {
            $reflection = new \ReflectionClass($class);
        }
        $actions = [];
        $methods = $reflection->getMethods();
        foreach ($methods as $method) {
            $methodName = $method->getName();
            $count = strlen($methodName);
            if (substr($methodName, -6) === 'Action') {
                $actions[] = substr($method->getName(), 0, $count - 6);
            }
        }

        return $actions;
    }

    /**
     * @param $controllers
     * @param $bundle
     * @param $controller
     * @param $action
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderWithTwig($controllers, $bundle, $controller, $action)
    {
        return $this->render('DarkMagicRouterBundle:Default:index.html.twig', [
            'controllers' => $controllers,
            'bundle' => $bundle,
            'controller' => $controller,
            'action' => $action,
        ]);
    }

    protected function resolveServiceDefinition($serviceId) {
        if ($this->containerBuilder->hasDefinition($serviceId)) {
            return $this->containerBuilder->getDefinition($serviceId);
        }

        // Some service IDs don't have a Definition, they're simply an Alias
        if ($this->containerBuilder->hasAlias($serviceId)) {
            return $this->containerBuilder->getAlias($serviceId);
        }

        // the service has been injected in some special way, just return the service
        return $this->containerBuilder->get($serviceId);
    }

    protected function getContainerBuilder() {
        if (!is_file($cachedFile = $this->container->getParameter('debug.container.dump'))) {
            throw new \LogicException(sprintf(
                'Debug information about the container could not be found. Please clear the cache and try again.'
            ));
        }

        $container = new ContainerBuilder();

        $loader = new XmlFileLoader($container, new FileLocator());
        $loader->load($cachedFile);

        return $container;
    }

    protected function fetchServicesControllers()
    {
        $this->containerBuilder = $this->getContainerBuilder();
        $controller_list = $this->container->getServiceIds();
        $servicesList = $services = [];
        foreach ($controller_list as $id) {
            $definition = $this->resolveServiceDefinition($id);
            if ($definition instanceof Definition) {
                $className = $definition->getClass();
                if (substr($className, -10) === 'Controller') {
                    $servicesList[$id] = $className;
                    $services[$id] = $definition;
                }
            }
        }
        $this->servicesList = $servicesList;
        $this->services = $services;
    }

    /**
     * @param Request $request
     * @param $controllers
     * @param $bundle
     * @param $controller
     * @param $action
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function callController(Request $request, $controllers, $bundle, $controller, $action)
    {
        $controllers = array_filter($controllers, function ($value) use ($bundle, $controller, $action) {
            $found = false;
            foreach ($value['actions'] as $item) {
                if ($action === $item) {
                    $found = true;
                }
            }

            return $value['bundle_short'] === $bundle
            && $value['controller'] === $controller
            && $found;
        });
        $controllerItem = array_pop($controllers);
        if ($controllerItem['service']) {
            $fullAction = $action . 'Action';
            $service = $this->container->get($controllerItem['service']);
            $reflection = new \ReflectionClass($service);
            $required = $reflection->getMethod($fullAction)->getNumberOfRequiredParameters();
            if ($required > 1) {
                // TODO: here can be a query arguments
                throw new \BadMethodCallException(
                    'Sorry, the requested action required some special arguments. This possibility not implemented yet.'
                );
            }
            $response = $service->$fullAction($request);
        } else {
            $arguments = []; // TODO: here can be a query arguments
            $response = $this->forward($bundle . ':' . $controller . ':' . $action, $arguments);
        }

        return $response;
    }
}
