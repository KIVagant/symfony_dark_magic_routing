Symfony Dark Magic Router
====================

## The task:

Create an working Symfony routing with the mask: /{buldle}/{controller}/{action}

## Installation

```
composer install
php bin/console server:run
```

Then open the link: http://127.0.0.1:8000

## Limitations

Some controller actions requires special arguments. This feature is not implemented, but can be (with query arguments).